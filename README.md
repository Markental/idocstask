# IDocs task
Test task for Junior Backend developer position in IDocs

## Tecnologies used:
- ASP .Net Core 5 (EntityFrameworkCore 5)
- Database - MS SQL, MongoDB (GridFS)
- Swagger
- epplus (excel report)
- XUnit

#### Contact me
- [LinkedIn]
- [Telegram]
- E-mail: [karataevolzhas@gmail.com]

[Node.js npm]: <https://nodejs.org/en/>
[Telegram]: <https://t.me/Markental>
[LinkedIn]: <https://www.linkedin.com/in/olzhas-karatayev/>
[karataevolzhas@gmail.com]: <mailto:karataevolzhas@gmail.com>
[Web Api]: <https://akvelontasktracker.azurewebsites.net/swagger/index.html>
[Angular Client Application]: <https://akvelontasktrackerclient.azurewebsites.net/>
