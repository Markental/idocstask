﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using IDocs.Core.Models.Identity;

namespace IDocs.Core.Infrastructure.Extensions
{
    public static class IdentityExtensions
    {
        public static CurrentUserModel GetCurrentUser(this ClaimsPrincipal user)
        {
            var currentUser = new CurrentUserModel
            {
                Id = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value,
                UserName = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value,
                FirstName = user.Claims.FirstOrDefault(c => c.Type == "FirstName")?.Value,
                LastName = user.Claims.FirstOrDefault(c => c.Type == "LastName")?.Value,
                Role = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value,
                AccessToken = user.Claims.FirstOrDefault(c => c.Type == "access_token")?.Value
            };

            return currentUser;
        }
    }

}

