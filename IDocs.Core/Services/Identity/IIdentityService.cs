﻿using System.Collections.Generic;
using System.Security.Claims;

namespace IDocs.Core.Services
{
    public interface IIdentityService
    {
        string GenerateJwtToken(string userId, string userName, string secret);
    }
}
