﻿using IDocs.Core.ContextEntities;
using IDocs.Core.Models.File;
using IDocs.Core.Models.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDocs.Core.Services.Repositories;
using static IDocs.Core.Abstractions.Enums;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;

namespace IDocs.Core.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly ApplicationDbContext _context;
        private readonly FileRepository _fileRepository;
        public DocumentService(ApplicationDbContext context,
            FileRepository fileRepository) 
        {
            _context = context;
            _fileRepository = fileRepository;
        }
        public async Task<Document> GetOneById(Guid id)
        {
            var deletedType = await _context.DocumentTypes
                .FirstOrDefaultAsync(x => x.Flag == DocumentTypesEnum.DELETED);

            var document = await _context.Documents
                .Include(x => x.Reciever)
                .Include(x => x.DocumentType)
                .Include(x => x.CreatedBy)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id && x.DocumentTypeId != deletedType.Id);

            return document;
        }

        public async Task<IEnumerable<Document>> GetAllDocuments() 
        {
            var deletedType = await _context.DocumentTypes
                    .FirstOrDefaultAsync(x => x.Flag == DocumentTypesEnum.DELETED);

            var documents = await _context.Documents
                .Where(x => x.DocumentTypeId != deletedType.Id)
                .Include(x => x.DocumentType)
                .Include(x => x.Reciever)
                .Include(x => x.CreatedBy)
                .AsNoTracking()
                .ToListAsync();

            return documents;
        }

        public async Task<IEnumerable<Document>> GetDocumentsByCreatorId(string id) 
        {
            var deletedType = await _context.DocumentTypes
                .FirstOrDefaultAsync(x => x.Flag == DocumentTypesEnum.DELETED);

            var documents = await _context.Documents
                    .Include(x => x.Reciever)
                    .Include(x => x.DocumentType)
                    .Include(x => x.CreatedBy)
                    .Where(x => x.CreatedById == id && x.DocumentTypeId != deletedType.Id)
                    .AsNoTracking()
                    .ToListAsync();

            return documents;
        }

        public async Task<IEnumerable<Document>> GetDocumentsByRecieverId(string id) 
        {
            var deletedType = await _context.DocumentTypes
                .FirstOrDefaultAsync(x => x.Flag == DocumentTypesEnum.DELETED);

            var documents = await _context.Documents
                    .Include(x => x.Reciever)
                    .Include(x => x.DocumentType)
                    .Include(x => x.CreatedBy)
                    .Where(x => x.RecieverId == id && x.DocumentTypeId != deletedType.Id)
                    .AsNoTracking()
                    .ToListAsync();

            return documents;
        }

        public async Task<(IEnumerable<Document> Collection, int TotalCount)> GetFilteredList(
            string name,
            Guid? typeId,
            DateTime? dateFrom,
            DateTime? dateTo,
            int pageNumber,
            int pageSize)
        {
            var deletedType = await _context.DocumentTypes
                .FirstOrDefaultAsync(x => x.Flag == DocumentTypesEnum.DELETED);

            IQueryable<Document> query = _context.Documents
                .Where(x => x.DocumentTypeId != deletedType.Id)
                .OrderByDescending(x => x.CreatedOn);

            if (!string.IsNullOrWhiteSpace(name))
            {
                string lowerName = name.ToLower();
                query = query
                    .Where(x => x.Name.ToLower().Contains(lowerName));
            }

            if (typeId != null)
            {
                query = query
                   .Where(x => x.DocumentTypeId == typeId);
            }

            if (dateFrom != null)
            {
                query = query.Where(x => x.CreatedOn >= dateFrom);
            }

            if (dateTo != null)
            {
                query = query.Where(x => x.CreatedOn <= dateTo);
            }

            var totalCount = query.Count();

            query = query.Skip(pageNumber * pageSize);

            query = query.Take(pageSize);

            var result = await query
                .Include(x => x.CreatedBy)
                .Include(x => x.Reciever)
                .Include(x => x.DocumentType)
                .AsNoTracking()
                .ToListAsync();

            return (result, totalCount);
        }

        public async Task<Guid> Create(
            string name,
            Guid documentTypeId,
            string reciever,
            IFormFile file,
            CurrentUserModel creator) 
        {
            var result = await AddFileToMongo(file);

            var document = new Document
            {
                Name = name,
                FileName = file.FileName,
                MimeType = file.ContentType,
                Extension = Path.GetExtension(file.FileName),
                DocumentTypeId = documentTypeId,
                CreatedById = creator.Id,
                CreatedOn = DateTime.Now,
                StorageId = result.storageId.ToString(),
                Size = result.size
            };

            _context.Documents.Add(document);

            await _context.SaveChangesAsync();

            return document.Id;
        }

        public async Task<bool> Edit(
            Guid documentId,
            string name,
            IFormFile newFile,
            CurrentUserModel currentUser) 
        {
            var document = await _context.Documents
                .FirstOrDefaultAsync(x => x.Id == documentId);

            bool isChanged = false;

            if (document.CreatedById != currentUser.Id) 
            {
                return isChanged;
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                document.Name = name;
                isChanged = true;
            }

            if(newFile != null) 
            {
                var result = await UpdateFileInMongo(document.StorageId, newFile);
                document.StorageId = result.storageId.ToString();
                document.MimeType = newFile.ContentType;
                document.FileName = newFile.FileName;
                document.Extension = Path.GetExtension(newFile.FileName);
                document.Size = result.size;
                isChanged = true;
            }

            await _context.SaveChangesAsync();

            return isChanged;
        }

        public async Task<bool> Delete(Guid id, CurrentUserModel user)
        {
            var document = await _context.Documents.FindAsync(id);

            if (document == null)
            {
                return false;
            }

            if (document.CreatedById != user.Id)
            {
                return false;
            }
            document.DocumentTypeId = _context.DocumentTypes.FirstOrDefault(x => x.Flag == DocumentTypesEnum.DELETED).Id;
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<byte[]> GenerateReport() 
        {
            DataTable dtData = new DataTable();
            dtData.Columns.Add("Document ID");
            dtData.Columns.Add("Name");
            dtData.Columns.Add("Document Type");
            dtData.Columns.Add("Reciever");
            dtData.Columns.Add("File Size");
            dtData.Columns.Add("File Name");
            dtData.Columns.Add("Mime Type");
            dtData.Columns.Add("Extension");
            dtData.Columns.Add("Created On");
            dtData.Columns.Add("Created By");

            var documents = await GetAllDocuments();

            foreach (var document in documents) 
            {
                DataRow row = dtData.NewRow();
                row["Document ID"] = document.Id;
                row["Name"] = document.Name;
                row["Document Type"] = document.DocumentType.Name;
                row["Reciever"] = document.Reciever.FullName();
                row["File Size"] = document.Size;
                row["File Name"] = document.FileName;
                row["Mime Type"] = document.MimeType;
                row["Extension"] = document.Extension;
                row["Created On"] = document.CreatedOn;
                row["Created By"] = document.CreatedBy.FullName();
                dtData.Rows.Add(row);
            }

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var package = new ExcelPackage())
            {

                var sheet = package.Workbook.Worksheets
                    .Add("Documents Report");

                int i = 1, j = 1;

                // Heading row
                foreach (DataColumn col in dtData.Columns)
                {
                    sheet.Cells[i, j].Value = col.Caption;
                    sheet.Cells[i, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    j++;
                }

                i++; j = 1;

                foreach (DataRow row in dtData.Rows)
                {
                    foreach (DataColumn col in dtData.Columns)
                    {
                        sheet.Cells[i, j].Value = row[col];
                        j++;
                    }
                    i++;
                    j = 1;
                }

                sheet.Protection.IsProtected = true;

                return await package.GetAsByteArrayAsync();
            }
        }

        public async Task<IEnumerable<DocumentType>> GetTypes() 
        {
            return await _context.DocumentTypes
                .AsNoTracking()
                .ToListAsync();
        }

        private async Task<(ObjectId storageId, int size)> AddFileToMongo(IFormFile file) 
        {
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();

                var createResult = await _fileRepository.GridFsUploadAsync(file.FileName, fileBytes);

                return (createResult, fileBytes.Length);
            }
        }

        private async Task<(ObjectId storageId, int size)> UpdateFileInMongo(string oldFileId, IFormFile newFile) 
        {
            using (var ms = new MemoryStream()) 
            {
                await _fileRepository.GridFsDeleteAsync(oldFileId);
                newFile.CopyTo(ms);
                var fileBytes = ms.ToArray();

                var newStorageId = await _fileRepository.GridFsUploadAsync(newFile.FileName, fileBytes);

                return (newStorageId, fileBytes.Length);
            }
        }
    }
}
