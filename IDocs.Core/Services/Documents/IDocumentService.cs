﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDocs.Core.ContextEntities;
using IDocs.Core.Models.Identity;
using Microsoft.AspNetCore.Http;

namespace IDocs.Core.Services
{
    public interface IDocumentService
    {
        Task<Guid> Create(string name, Guid documentTypeId, string reciever, IFormFile file, CurrentUserModel creator);
        Task<bool> Delete(Guid id, CurrentUserModel user);
        Task<bool> Edit(Guid documentId, string name, IFormFile newFile, CurrentUserModel currentUser);
        Task<byte[]> GenerateReport();
        Task<IEnumerable<Document>> GetAllDocuments();
        Task<IEnumerable<Document>> GetDocumentsByCreatorId(string id);
        Task<IEnumerable<Document>> GetDocumentsByRecieverId(string id);
        Task<(IEnumerable<Document> Collection, int TotalCount)> GetFilteredList(string name, Guid? typeId, DateTime? dateFrom, DateTime? dateTo, int pageNumber, int pageSize);
        Task<Document> GetOneById(Guid id);
        Task<IEnumerable<DocumentType>> GetTypes();
    }
}
