﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System.Security.Authentication;
using System.Threading.Tasks;
using IDocs.Core.Models.File;
using IDocsAPI.Core.ApplicationSettingModels;

namespace IDocs.Core.Services.Repositories
{
    public class FileRepository
    {
        private readonly MongoClient _mongoClient;
        private readonly IMongoDatabase _database;
        private readonly MongoStorageSettings _mongoSettings;
        private readonly IMongoCollection<FileBinaryModel> _mongoCollection;
        private readonly IGridFSBucket _gridFS;

        public FileRepository(IOptions<MongoStorageSettings> options)
        {
            _mongoSettings = options.Value;

            MongoClientSettings settings = MongoClientSettings.FromUrl(
              new MongoUrl(_mongoSettings.ConnectionString)
            );

            settings.SslSettings =
                new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };

            

            _mongoClient = new MongoClient(settings);
            _database = _mongoClient.GetDatabase(_mongoSettings.DatabaseName);
            _gridFS = new GridFSBucket(_database, new GridFSBucketOptions 
            {
                BucketName = "Files",
                ChunkSizeBytes = 1048576 * 2   // 2 mb
            });
            _mongoCollection = _database.GetCollection<FileBinaryModel>("files-table");
        }

        public async Task<FileBinaryModel> CreateAsync(FileBinaryModel item)
        {
            await _mongoCollection.InsertOneAsync(item);
            return item;
        }

        public async Task<ObjectId> GridFsUploadAsync(string filename, byte[] content) 
        {
            var result = await _gridFS.UploadFromBytesAsync(filename, content);
            return result;
        }

        public async Task<FileBinaryModel> GetAsync(string id)
        {
            var objectId = ObjectId.Parse(id);
            var read = await _mongoCollection.FindAsync(p => p.Id == objectId);
            return await read.SingleOrDefaultAsync();
        }

        public async Task<byte[]> GridFsGetAsync(string id) 
        {
            var fileContent = await _gridFS.DownloadAsBytesAsync(ObjectId.Parse(id));
            return fileContent;
        }

        public async Task DeleteAsync(string id)
        {
            var objectId = ObjectId.Parse(id);
            var deleteResult = await _mongoCollection.DeleteOneAsync(p => p.Id == objectId);
        }

        public async Task GridFsDeleteAsync(string id) 
        {
            var objectId = ObjectId.Parse(id);
            await _gridFS.DeleteAsync(objectId);
        }

        public async Task UpdateContent(string id, byte[] updatedContent) 
        {
            var objectId = ObjectId.Parse(id);
            var filter = Builders<FileBinaryModel>.Filter.Eq("Id", objectId);
            var update = Builders<FileBinaryModel>.Update.Set("Content", updatedContent);
            await _mongoCollection.UpdateOneAsync(filter, update);
        }
    }
}
