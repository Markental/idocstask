﻿using System.ComponentModel.DataAnnotations;

namespace IDocs.Core.Models.Identity
{
    public class ChangePasswordRequestModel
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
