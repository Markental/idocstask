﻿using System.Collections.Generic;

namespace IDocs.Core.Models.Identity
{
    public class CurrentUserModel
    {
        public string FullName()
            => LastName + " " + FirstName;

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Id { get; set; }

        public string Role { get; set; }

        public string AccessToken { get; set; }
    }
}
