﻿using System.ComponentModel.DataAnnotations;

namespace IDocs.Core.Models
{
    public class RegisterRequestModel
    {
        [Required]
        [RegularExpression(@"^\d{12}$")] // exactly 12 digits
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}
