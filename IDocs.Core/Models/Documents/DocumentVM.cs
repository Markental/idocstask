﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDocs.Core.Models.Documents
{
    public class DocumentVM
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public Guid DocumentTypeId { get; set; }

        public string RecieverId { get; set; }

        public IFormFile File { get; set; }
    }
}
