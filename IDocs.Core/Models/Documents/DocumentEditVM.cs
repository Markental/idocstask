﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDocs.Core.Models.Documents
{
    public class DocumentEditVM
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public IFormFile File { get; set; }
    }
}
