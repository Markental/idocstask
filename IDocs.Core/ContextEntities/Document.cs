﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDocs.Core.ContextEntities
{
    [Table("Documents")]
    public class Document
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }

        [ForeignKey("DocumentType")]
        public Guid DocumentTypeId { get; set; }
        public virtual DocumentType DocumentType { get; set; }

        public string RecieverId { get; set; }
        public ApplicationUser Reciever { get; set; }

        /// <summary>
        /// Mongo id/Azure blob id/physical path
        /// </summary>
        public string StorageId { get; set; }

        /// <summary>
        /// Byte size
        /// </summary>
        public int Size { get; set; }

        [MaxLength(300)]
        public string FileName { get; set; }

        [StringLength(100)]
        public string MimeType { get; set; }

        [StringLength(100)]
        public string Extension { get; set; }


        public DateTime CreatedOn { get; set; }

        [ForeignKey("Creator")]
        public string CreatedById { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        
    }
}
