﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDocs.Core.ContextEntities
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }

        public string FullName()
            => LastName + " " + FirstName;

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public virtual IEnumerable<Document> CreatedDocuments { get; set; }

        public virtual IEnumerable<Document> RecievedDocuments { get; set; }

    }
}
