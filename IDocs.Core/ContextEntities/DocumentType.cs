﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IDocs.Core.Abstractions.Enums;

namespace IDocs.Core.ContextEntities
{
    [Table("DocumentTypes")]
    public class DocumentType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public DocumentTypesEnum Flag { get; set; }

        public virtual IEnumerable<Document> Documents { get; set; }
    }
}
