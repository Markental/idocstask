﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IDocs.Core.ContextEntities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .HasMany(l => l.CreatedDocuments)
                .WithOne(m => m.CreatedBy);

            builder.Entity<ApplicationUser>()
                .HasMany(l => l.RecievedDocuments)
                .WithOne(m => m.Reciever);

        }

        #region DBSets
        public DbSet<Document> Documents { get; set; }
        
        public DbSet<DocumentType> DocumentTypes { get; set; }
        #endregion
    }
}
