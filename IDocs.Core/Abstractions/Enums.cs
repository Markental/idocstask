﻿namespace IDocs.Core.Abstractions
{
    public class Enums
    {
        public enum DocumentTypesEnum 
        {
            DELETED = 0,
            JURIDICAL = 1,
            OFFICIAL = 2,
            ACCOUNTING = 3
        }
    }
}
