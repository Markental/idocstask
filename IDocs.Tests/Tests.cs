using IDocs.Core.Models;
using IDocsAPI;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IDocs.Tests
{
    public class Tests
    {
        [Theory]
        [InlineData("/api/Identity/Login")]
        public async Task Post_IdentityController_LoginAction(string url)
        {
            var application = new WebApplicationFactory<Startup>()
        .WithWebHostBuilder(builder =>
        {
            
        });
            // Arange
            var client = application.CreateClient();
            var user = new LoginRequestModel
            {
                Username = "012345678910",
                Password = "Test01!"
            };
            var json = JsonConvert.SerializeObject(user);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            //Act
            var response = await client.PostAsync(url, data);
            var result = await response.Content.ReadAsStringAsync();

            //Assert
            Assert.Contains("token", result);
        }
    }
}
