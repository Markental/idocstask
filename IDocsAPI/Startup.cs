using IDocs.Core.ContextEntities;
using IDocs.Core.Infrastructure.Extensions;
using IDocs.Core.Infrastructure.Logging;
using IDocs.Core.Services;
using IDocs.Core.Services.Repositories;
using IDocsAPI.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace IDocsAPI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public Startup(IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(_configuration.GetConnectionString("SQLServerConnection")))
                .AddCors()
                .AddCustomIdentity()
                .AddAuthorization()
                .AddJwtAuthentication(services.GetApplicationSettings(_configuration))
                .AddCustomControllers(_webHostEnvironment.IsDevelopment())
                .AddCustomSwaggerGen();
                

            services
                .AddTransient<IIdentityService, IdentityService>()
                .AddTransient<IDocumentService, DocumentService>()
                //.AddTransient<IRecieversListService, RecieversListService>()
                .AddTransient<FileRepository>();

            services.AddHostedService<SeedWorker>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Program> logger)
        {
            app
                //.UseDeveloperExceptionPage()
                .UseCustomSwaggerUI()
                .UseMiddleware<RequestLoggingMiddleware>()
                .UseCors(x => x
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader())
                .UseHttpsRedirection()
                .UseRouting()
                //.UseAuthorization()
                .UseAuthentication()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}
