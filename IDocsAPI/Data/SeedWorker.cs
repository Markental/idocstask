﻿using IDocs.Core.ContextEntities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IDocsAPI.Data
{
    public class SeedWorker : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;

        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private ApplicationDbContext _context;
        //private IApprovalListService _listService;
        public SeedWorker(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken) 
        {
            using var scope = _serviceProvider.CreateScope();

            _context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            await _context.Database.EnsureCreatedAsync(cancellationToken);

            _userManager = scope.ServiceProvider
                .GetRequiredService<UserManager<ApplicationUser>>();

            _roleManager = scope.ServiceProvider
                .GetRequiredService<RoleManager<ApplicationRole>>();

            await CreateDocumentTypes();
        }

        private async Task CreateDocumentTypes()
        {
            var hasStatuses = _context.DocumentTypes.Any();
            if (!hasStatuses)
            {
                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "DELETED",
                    Flag = IDocs.Core.Abstractions.Enums.DocumentTypesEnum.DELETED
                });
                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "JURIDICAL",
                    Flag = IDocs.Core.Abstractions.Enums.DocumentTypesEnum.JURIDICAL
                });
                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "OFFICIAL",
                    Flag = IDocs.Core.Abstractions.Enums.DocumentTypesEnum.OFFICIAL
                });
                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "ACCOUNTING",
                    Flag = IDocs.Core.Abstractions.Enums.DocumentTypesEnum.ACCOUNTING
                });
            }
            await _context.SaveChangesAsync();
        }
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
