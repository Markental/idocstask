﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System;
using IDocs.Core.Models.Identity;
using IDocs.Core.ContextEntities;
using IDocs.Core.Services;
using IDocsAPI.Core.ApplicationSettingModels;
using IDocs.Core.Models;
using IDocs.Core.Infrastructure.Extensions;

namespace IDocsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentityController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IIdentityService identityService;
        private readonly ApplicationSettings appSettings;

        public IdentityController(
            UserManager<ApplicationUser> userManager,
            IIdentityService identityService,
            IOptions<ApplicationSettings> appSettings)
        {
            this.userManager = userManager;
            this.identityService = identityService;
            this.appSettings = appSettings.Value;
        }

        //[Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost]
        [Route(nameof(ChangePassword))]
        public async Task<ActionResult> ChangePassword(ChangePasswordRequestModel model) 
        {
            var currentUser = User.GetCurrentUser();

            var user = await userManager.FindByIdAsync(currentUser.Id);
            if (user == null) 
            {
                return NotFound();
            }

            var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

            if(result.Succeeded) 
            {
                return Ok();
            }

            return BadRequest(result.Errors);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(nameof(Register))]
        public async Task<ActionResult> Register(RegisterRequestModel model)
        {
            var user = new ApplicationUser
            {
                UserName = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName
            };
            var result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                return Ok();
            }

            return BadRequest(result.Errors);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(nameof(Login))]
        public async Task<ActionResult> Login(LoginRequestModel model)
        {
            var user = await userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                return BadRequest("User does not exist");
            }

            var passwordValid = await userManager.CheckPasswordAsync(user, model.Password);
            if (!passwordValid)
            {
                return BadRequest("Login or password is incorrect");
            }

            var claims = new List<Claim>();
            claims.Add(new Claim("FirstName", user.FirstName));
            claims.Add(new Claim("LastName", user.LastName));

            var token = identityService.GenerateJwtToken(userId: user.Id,
                userName: user.UserName,
                secret: appSettings.Secret);

            return Ok(new { token, user.UserName, FullName = user.FullName(), user.Id, claims });
        }

    }
}
