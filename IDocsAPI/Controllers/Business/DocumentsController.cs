﻿using IDocs.Core.ContextEntities;
using IDocs.Core.Infrastructure.Extensions;
using IDocs.Core.Models.Documents;
using IDocs.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDocsAPI.Controllers.Business
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : Controller
    {
        private readonly IDocumentService _documentService;

        public DocumentsController(IDocumentService documentService) 
        {
            _documentService = documentService;
        }

        [HttpGet]
        [Route(nameof(GetDocumentById) + "/{documentId}")]
        public async Task<Document> GetDocumentById([FromRoute] Guid documentId) 
        {
            var document = await _documentService.GetOneById(documentId);

            return document;
        }

        [HttpGet]
        [Route(nameof(GetDocumentsByCreatorId) + "/{creatorId}")]
        public async Task<IEnumerable<Document>> GetDocumentsByCreatorId([FromRoute] string creatorId)
        {
            var documents = await _documentService.GetDocumentsByCreatorId(creatorId);

            return documents;
        }

        [HttpGet]
        [Route(nameof(GetDocumentsByRecieverId) + "/{recieverId}")]
        public async Task<IEnumerable<Document>> GetDocumentsByRecieverId([FromRoute] string recieverId)
        {
            var documents = await _documentService.GetDocumentsByRecieverId(recieverId);

            return documents;
        }

        [HttpGet]
        [Route(nameof(GetFilteredList))]
        public async Task<(IEnumerable<Document> Collection, int TotalCount)> GetFilteredList(
            [FromQuery] string name,
            [FromQuery] Guid? documentTypeId,
            [FromQuery] DateTime? dateFrom,
            [FromQuery] DateTime? dateTo,
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            var result = await _documentService.GetFilteredList(
                name,
                documentTypeId,
                dateFrom,
                dateTo,
                skip.Value,
                top.Value);

            return result;
        }

        [HttpPost]
        [Route(nameof(Create))]
        public async Task<ActionResult<Guid>> Create([FromForm] DocumentVM model) 
        {
            var user = User.GetCurrentUser();

            if (model.File != null) 
            {
                // Check extension
                var extension = System.IO.Path.GetExtension(model.File.FileName);
                if (extension != ".pdf")
                {
                    return BadRequest("Файл должен быть .pdf");
                }
                // 10 MB Limit
                if (model.File.Length > 1024 * 1024 * 10)
                {
                    return BadRequest("Размер файла больше 10 МБ");
                }
            }

            var documentId = await _documentService.Create(
                model.Name,
                model.DocumentTypeId,
                model.RecieverId, 
                model.File,
                user);

            return CreatedAtAction(nameof(Create), new { DocumentId = documentId });
        }

        [HttpPatch]
        [Route(nameof(Edit))]
        public async Task<ActionResult> Edit([FromForm] DocumentEditVM model)
        {
            var user = User.GetCurrentUser();

            var isChanged = await _documentService.Edit(model.Id, model.Name, model.File, user);

            if (!isChanged) 
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// soft delete
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(nameof(Delete) + "/{documentId}")]
        public async Task<ActionResult> Delete([FromRoute] Guid documentId)
        {
            var user = User.GetCurrentUser();

            var success = await _documentService.Delete(documentId, user);

            if (success)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpGet]
        [Route(nameof(GenerateExcelReport))]
        public async Task<ActionResult> GenerateExcelReport() 
        {
            var result = await _documentService.GenerateReport();

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }
    }
}
