﻿using IDocs.Core.ContextEntities;
using IDocs.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDocsAPI.Controllers.Business
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentTypesController : Controller
    {
        private readonly IDocumentService _documentService;

        public DocumentTypesController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpGet]
        [Route(nameof(GetAll))]
        public async Task<IEnumerable<DocumentType>> GetAll()
        {
            return await _documentService.GetTypes();
        }
    }
}
